# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- LICENSE Apache 2.0.
- `packtpub-crawler` lib to crawl deal of the day.
- Add GitLab CI:
    - Compile project
    - Unit Tests
- Add `sbt-dynver` to manage versioning
- Add `sbt-scalafmt` plugin
