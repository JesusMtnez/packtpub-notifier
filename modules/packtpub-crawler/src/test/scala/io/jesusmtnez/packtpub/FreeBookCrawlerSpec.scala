package io.jesusmtnez.packtpub

import com.softwaremill.sttp.testing.SttpBackendStub
import com.softwaremill.sttp.{Id, _}
import io.jesusmtnez.packtpub.domain.Paths
import net.ruippeixotog.scalascraper.browser.{Browser, JsoupBrowser}
import org.scalatest.FlatSpec
import pureconfig.generic.auto._

class FreeBookCrawlerSpec extends FlatSpec {

  implicit val testingBackend: SttpBackend[Id, Nothing] =
    SttpBackendStub.synchronous.whenRequestMatchesPartial {
      case req if req.uri.path.endsWith("free-learning" :: Nil) =>
        Response.ok(os.read(os.resource / "packtpub20171120.html"))

      case _ =>
        Response.error("", 404)
    }

  implicit val B: Browser = JsoupBrowser()

  "Free Book Crawler" should "return a non empty deal with a valid web page" in {
    assert(crawler.getDealInfo.isDefined)
  }

  it should "return an empty deal with an invalid web page" in {
    implicit val wrongBackend: SttpBackend[Id, Nothing] =
      SttpBackendStub.synchronous.whenAnyRequest.thenRespond("")

    assert(crawler(wrongBackend).getDealInfo.isEmpty)
  }

  private def crawler(implicit SB: SttpBackend[Id, Nothing]): FreeBookCrawler =
    FreeBookCrawler(pureconfig.loadConfigOrThrow[Paths]("paths"))(B, SB)
}
