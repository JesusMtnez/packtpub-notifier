package io.jesusmtnez.packtpub.domain

final case class Paths(baseUrl: String, freeDeal: String, myEbooks: String)
final case class Auth(usermail: String, password: String)
final case class Download(location: String)
final case class CrawlerConfig(auth: Auth, paths: Paths, download: Download)
