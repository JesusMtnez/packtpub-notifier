package io.jesusmtnez.packtpub

import com.softwaremill.sttp.Uri.QueryFragment.KeyValue
import com.softwaremill.sttp.{Id, _}
import io.jesusmtnez.packtpub.domain._
import net.ruippeixotog.scalascraper.browser.Browser
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.model.Element

trait PacktpubLibraryCrawler {
  def myBooks: List[Book]
  def downloadBooks(books: List[Book]): Unit
}

object PacktpubLibraryCrawler {

  def apply(config: CrawlerConfig)(implicit B: Browser,
                                   SB: SttpBackend[Id, Nothing]): PacktpubLibraryCrawler =
    new PacktpubLibraryCrawler {

      override def myBooks: List[Book] = {

        val myBooksUri = Uri("https", config.paths.baseUrl, config.paths.myEbooks.split('/').toList)

        val myBooksInitialPageRaw: String =
          sttp
            .get(myBooksUri)
            .send()
            .body
            .getOrElse(throw new RuntimeException)

        val numberOfPages: Int =
          (B.parseString(myBooksInitialPageRaw) >> element(".solr-pager-page-selector")).children.size

        val bookElements: List[Element] =
          (1 to numberOfPages).toList
            .map(p => myBooksUri.queryFragment(KeyValue("page", p.toString)))
            .flatMap(sttp.get(_).send().body.toOption)
            .flatMap(B.parseString(_) >> element("#product-account-list") >> elementList(".unseen"))

        // TODO Use logger instead of println
        println(s"Found ${bookElements.size} books in your account (Pages: $numberOfPages)")

        bookElements
          .map { bookElement =>
            val title: String =
              (bookElement >> text(".title"))
                .replaceAll("/", "-")
                .replace(" [eBook]", "")
                .replaceAll(":", " -")

            val author: String = bookElement >> text(".author")

            val links: Map[Format, Uri] =
              (bookElement >> elementList(".product-buttons-line > div:nth-child(2) > a"))
                .foldLeft(Map.empty[Format, Uri]) { (formatLinkMap, aElement) =>
                  aElement >> element(".fake-button") >?> attr("format") match {
                    case Some("pdf") =>
                      formatLinkMap +
                        (PDF -> aElement >> attr("href").map(l =>
                          Uri("https", config.paths.baseUrl, l.split('/').toList)))
                    case Some("epub") =>
                      formatLinkMap +
                        (EPUB -> aElement >> attr("href").map(l =>
                          Uri("https", config.paths.baseUrl, l.split('/').toList)))
                    case Some("mobi") =>
                      formatLinkMap +
                        (MOBI -> aElement >> attr("href").map(l =>
                          Uri("https", config.paths.baseUrl, l.split('/').toList)))
                    case _ =>
                      (aElement >?> attr("href"))
                        .filter(_ != "#")
                        .fold(formatLinkMap)(l =>
                          formatLinkMap + (CODE -> Uri(java.net.URI.create(l))))
                  }
                }
            Book(title, author, links)
          }
      }

      override def downloadBooks(books: List[Book] = Nil): Unit =
        if (books.isEmpty) myBooks.foreach(downloadBook)
        else books.foreach(downloadBook)

      private def downloadBook(b: Book): Unit = {
        val downloadPath = os.root / config.download.location / b.title.replace(" ", "_")

        b.urls.map(
          e =>
            sttp
              .get(e._2)
              .response(asPath((downloadPath / b.title.concat(e._1.toString.toLowerCase)).toNIO)))
      }
    }
}
