package io.jesusmtnez.packtpub.domain

case class FreeBookDeal(
  title: String,
  summary: String,
  image: String,
) {
  override def toString: String = {
    s"""
       |Title: $title
       |Summary: $summary
       |ImageURL: $image
    """.stripMargin
  }
}
