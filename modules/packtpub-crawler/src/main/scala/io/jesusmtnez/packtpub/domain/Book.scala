package io.jesusmtnez.packtpub.domain

import com.softwaremill.sttp.Uri

sealed trait Format
case object PDF  extends Format
case object EPUB extends Format
case object MOBI extends Format
case object CODE extends Format

case class Book(title: String, author: String, urls: Map[Format, Uri])
