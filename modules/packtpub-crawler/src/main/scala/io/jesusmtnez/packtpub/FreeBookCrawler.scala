package io.jesusmtnez.packtpub

import com.softwaremill.sttp.{Id, _}
import io.jesusmtnez.packtpub.domain._
import net.ruippeixotog.scalascraper.browser.Browser
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.model.Document

trait FreeBookCrawler {

  /** Get Daily Deal information if available */
  def getDealInfo: Option[FreeBookDeal]

  /** Add daily deal to account */
  def getDeal: Boolean
}

object FreeBookCrawler {

  def apply(config: Paths)(implicit B: Browser, SB: SttpBackend[Id, Nothing]): FreeBookCrawler =
    new FreeBookCrawler {
      // TODO Improve error handling
      override def getDealInfo: Option[FreeBookDeal] =
        sttp
          .get(Uri("https", config.baseUrl, config.freeDeal.split('/').toList))
          .send()
          .body
          .fold(_ => Option.empty[FreeBookDeal], b => parseDealInfo(B.parseString(b)))

      // TODO Really implement getDeal method to try adding the deal to the account
      override def getDeal: Boolean = false

      private def parseDealInfo(doc: Document): Option[FreeBookDeal] =
        for {
          deal     <- doc >?> element("#deal-of-the-day")
          title    <- deal >?> text(".dotd-title")
          summary  <- deal >?> texts(".dotd-main-book-summary > div") map (_ mkString ". ")
          imageUrl <- deal >?> element(".bookimage") >> attr("src")
        } yield FreeBookDeal(title, summary, imageUrl)
    }

}
