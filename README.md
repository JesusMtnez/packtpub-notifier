[![license](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](/LICENSE) [![pipeline status](https://gitlab.com/JesusMtnez/packtpub-notifier/badges/master/pipeline.svg)](https://gitlab.com/JesusMtnez/packtpub-notifier/pipelines)

> Since Januray 2019, PacktPub has changed the webpage and free books are now only available through their reader web app. For that reason, I will discontinue this project.

# PacktPub Daily Free Book Notifier

The purpose of this project is notifying through multiple platform about the free dialy book PacktPub offers. To do so, the project is divived in multiple modules:

- **PacktPub Crawler:** a web crawler to get PacktPub Daily Free book information using [Scala Scraper](https://github.com/ruippeixotog/scala-scraper)
- `TODO` PacktPub Telegram
- `TODO` PacktPub Slack

## Roadmap

I tried to keep everything organized as I can, but you could keep track of the project roadmap here:

- Issues list: [here](https://gitlab.com/JesusMtnez/packtpub-notifier/issues)
- Issues board: [here](https://gitlab.com/JesusMtnez/packtpub-notifier/boards)
- Milestones: [here](https://gitlab.com/JesusMtnez/packtpub-notifier/milestones)

Feel free to open any issue for comments or suggestions.

## Changelog

All notable changes to this project will be documented in [CHANGELOG.md](/CHANGELOG.md).

## Copyright

Copyright (c) 2017-2018 JesusMtnez. See [LICENSE](/LICENSE) for details.
