lazy val root = (project in file("."))
  .settings(
    name := "packtpub-notifier",
    description := "PacktPub Notifier",
    organization := "io.jesusmtnez",
    licenses += "Apache-2.0" -> url("https://www.apache.org/licenses/LICENSE-2.0"),
    startYear := Some(2017),
    scalaVersion := "2.12.7",
    scalafmtOnCompile in ThisBuild := true
  )
  .aggregate(`packtpub-crawler`)

lazy val `packtpub-crawler` = (project in file("modules/packtpub-crawler"))
  .settings(
    name := "packtpub-crawler",
    description := "Small library to crawl daily PacktPub Free Deal",
    libraryDependencies ++= Seq(
      "net.ruippeixotog"      %% "scala-scraper" % "2.1.0",
      "com.softwaremill.sttp" %% "core"          % "1.5.0",
      "com.lihaoyi"           %% "os-lib"        % "0.2.2",
      "com.github.pureconfig" %% "pureconfig"    % "0.10.0" % "test",
      "org.scalatest"         %% "scalatest"     % "3.0.5" % "test"
    )
  )
